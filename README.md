# EV OBD Logging
This project uses Python code to send and receive CAN messages to a Nissan Leaf to collect and decode data to human readable format. Data will be saved locally while a network connection is unavailable, and will upload once a connection has been restored.

## Install Dependencies
Dependencies are required for the Python data collection script to run. For this, run:
```bash
pip3 install -r requirements.txt
```

Other packages needed for overall use can be obtained with the following commands:
```bash
sudo apt-get install -y openfortivpn
```

## Services:
Some system services are required for use on Raspberry Pi startup. These will need to be copied to necessary
folders, and enabled. This system services are:

## ble-autoconnect:
This service is used to autoconnect to the OBD scanner on startup. It uses settings specified in /ble-serial/helper/autoconnect.ini
Enable service with:

```bash
sudo cp /home/pi/python-projects/obd/services/ble-autoconnect-user.service \
/home/pi/.config/systemd/user/ble-autoconnect-user.service
systemctl --user daemon-reload
systemctl --user enable ble-autoconnect-user.service
```

## obd_logger_start:
This service is used to start the obd_logger.py on startup. First add your VPN credentials into /etc/openfortivpn/config
Once complete, run the VPN with:
```bash 
sudo openfortivpn
```
This will return a certificate that can then be added to the config file.
Enable the service with:

```bash
sudo cp /home/pi/python-projects/obd/services/obd_logger_start.service \
/home/pi/.config/systemd/user/obd_logger_start.service
systemctl --user daemon-reload
systemctl --user enable obd_logger_start.service
```

## openfortivpn:
This service is used to connect to VPN when internet connection is available. Transfer it to the proper location with:

```bash
sudo cp /home/pi/python-projects/obd/services/openfortivpn.service \
/usr/lib/systemd/system/openfortivpn.service
sudo systemctl daemon-reload
```

## If using uninterruptible power supply
The UPS is used to keep the Raspberry Pi powered on long enough to perform a data upload once the EV has been shut off.
ups.sh is a script that contains the procedure the Pi will follow once power has been disconnected for 5 seconds.
If you alter the contents of ups.sh in the repository, be sure to update the ups.sh script actually being used by the Pi:

```bash
sudo cp /home/pi/python-projects/obd/scripts/ups.sh /usr/bin/
```
