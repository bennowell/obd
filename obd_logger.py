#!/usr/bin/env python3

# Comment made locally!
# Comment made remotely!
# Comment made on laptop
# this should be in feature-add-ev-id branch

# importing modules
import serial
import logging
import time
from threading import Thread
import yaml
import os
from os import path
import csv
from datetime import datetime
import mysql.connector as sql
from mysql.connector import Error
import csv
import configparser
import sshtunnel
from sshtunnel import SSHTunnelForwarder

#Set SSH Tunnel Timeout
sshtunnel.TUNNEL_TIMEOUT = 10.0
sshtunnel.SSH_TIMEOUT = 10.0

# Create and configure logger
logging.basicConfig(filename='/home/pi/python-projects/obd/logs/obd_logger_output.log', 
					format='%(asctime)s %(message)s', 
					filemode='w')

# Create object
logger = logging.getLogger()

# Set threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)
logger.info("Logging file created")

# Define function for writing to OBD2 scanner
def can_request(request_id, string_of_bytes):
   # Set CAN ID
   logger.info('CAN ID: {}'.format(hex(request_id)))
   at_sh_message = b'AT SH %b\r' % hex(request_id)[2:].encode()
   logger.info(at_sh_message)
   ser.write(at_sh_message)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logger.info(ser_bytes)
    
   at_fc_sh_message = b'AT FC SH %b\r' % hex(request_id)[2:].encode()
   logger.info(at_fc_sh_message)
   ser.write(at_fc_sh_message)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logger.info(ser_bytes)
   
   # Set flow control mode
   logger.info('Setting flow control mode')
   flow_control_mode = b'AT FC SM 1\r'
   logger.info(flow_control_mode)
   ser.write(flow_control_mode)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logger.info(ser_bytes)
	
   # Send CAN request
   logger.info('Sending message: {}'.format(string_of_bytes))
   msg_bytes = b'%b\r' % string_of_bytes.encode()
   logger.info(msg_bytes)
   ser.write(msg_bytes)
   time.sleep(0.1)

# Define function for data upload with VPN
# If data gets uploaded successfully, 'uploaded' is set to True, which will clear the log file
# If data doesn't get uploaded successfully, 'uploaded' remains False, which will append the log file
def upload_vpn(outfile, fieldnames, filename):
	uploaded = False
	logger.info('Starting VPN')
	try:
		os.system('sudo bash /home/pi/python-projects/obd/scripts/start-openfortivpn.sh')
		time.sleep(30)
		logger.info('Started openfortivpn')

		logger.info('Creating SSH tunnel')
		try:
			with SSHTunnelForwarder(
				(ssh_host, ssh_port),
				ssh_username=ssh_user,
				remote_bind_address=(mysql_hostname, mysql_port)) as tunnel:
				logger.info(tunnel.local_bind_address)
				logger.info('Connecting to MySQL through SSH tunnel')
				mydb = None
				try:
					mydb = sql.connect(host='localhost', user=mysql_username, password=mysql_password, database=mysql_main_database, port=tunnel.local_bind_port, auth_plugin = 'mysql_native_password')
					logger.info('Connected to MySQL')
					if mydb.is_connected():
						logger.info('Connected to MySQL database, uploading')
						outfile.close()

						query_placeholders = ', '.join(['%s'] * len(fieldnames))
						query_columns = ', '.join(fieldnames)

						with open(filename + '.csv') as csv_data:
							cursor = mydb.cursor()
							reader = csv.reader((line.replace('\0','') for line in csv_data))
							next(reader)
						
							for row in reader:
								logger.info(row)
								for i in row:
									breaker = False
									if not i:
										logger.info('Found empty element, skipping MySQL upload')
										breaker = True
										break
								if breaker:
									continue
								logger.info('Uploading row')
								insert_query = '''INSERT INTO %s (%s) VALUES (%s) ''' %(table_name, query_columns, query_placeholders)
								cursor.execute(insert_query, row)
								mydb.commit()

						logger.info('Data uploaded to MySQL')
						uploaded = True

				except Error as e:
					logger.info(e)
					logger.info("Skipping data upload: couldn't connect to MySQL")

				finally:
					if mydb is not None and mydb.is_connected():
						mydb.close()
		
		except (Exception, KeyboardInterrupt) as e:
			logger.info(e)
			logger.info("Skipping data upload: couldn't establish SSH tunnel")

	except Exception as vpn_error:
		logger.info(vpn_error)
		logger.info("Skipping data upload: couldn't start openfortivpn")
	
	return uploaded	
# Define function for data upload with no VPN
def upload(hostname, username, db, password, outfile, fieldnames, filename):
	uploaded = False
	mydb = None
	try:
		mydb = sql.connect(host=hostname, database=db, user=username, password=password, connect_timeout=5)
		
		if mydb.is_connected():
			logger.info('Connected to MySQL database, uploading')
			outfile.close()

			query_placeholders = ', '.join(['%s'] * len(fieldnames))
			query_columns = ', '.join(fieldnames)

			with open(filename + '.csv') as csv_data:
				cursor = mydb.cursor()
				reader = csv.reader((line.replace('\0','') for line in csv_data))
				next(reader)
					
				for row in reader:
					logger.info(row)
					for i in row:
						breaker = False
						if not i:
							logger.info('Found empty element, skipping MySQL upload')
							breaker = True
							break
					if breaker:
						continue
					logger.info('Uploading row')
					insert_query = '''INSERT INTO %s (%s) VALUES (%s) ''' %(table_name, query_columns, query_placeholders)
					cursor.execute(insert_query, row)
					mydb.commit()

			logger.info('Data uploaded to MySQL')
			uploaded = True
			
	except Error as e:
		logger.info(e)
		logger.info("Skipping data upload: couldn't connect to MySQL")

	finally:
		if mydb is not None and mydb.is_connected():
			mydb.close()

	return uploaded

# Load database INI file
db_config = configparser.ConfigParser()
db_config.read("/home/pi/python-projects/obd/config/database.ini")
logger.info('Found database file')

# Check if using VPN for connection to database
using_vpn = db_config['SETTINGS'].getboolean('Using_VPN')
if using_vpn == True:
	logger.info('Using VPN, will use SSH tunnel')
	ssh_host = db_config['SERVER']['SSH_Host']
	ssh_user = db_config['SERVER']['SSH_User']
	ssh_port = int(db_config['SERVER']['SSH_Port'])

# Database information
database = db_config['MYSQL']
mysql_hostname = database['MySQL_Hostname']
mysql_username = database['MySQL_Username']
mysql_password = database['MySQL_Password']
mysql_main_database = database['MySQL_Database']
mysql_port = int(database['MYSQL_Port'])
table_name = database['MySQL_Table']

# Load YAML config data, assign necessary variables
logger.info('Loading config...')
with open('/home/pi/python-projects/obd/config/obd_logger_config.yaml') as f:
	config = yaml.load(f, Loader=yaml.FullLoader)
	
# Get EV ID
ev_id = config['settings']['ev_id']
# Timeout attemps
timeout_attempts = config['settings']['timeout_attempts']
# Sampling time
sampling_time = config['settings']['timeout_attempts']
# Determine any data being requested
requested_data = config['data']['requested_data']
# Determine any data being passively listened for
passive_data = config['data']['passive_data']

fieldnames = ['date_time', 'ev_id', 'count']
response_ids = []
# Determine fieldnames and response IDs of data requested, if any
if requested_data:
	logger.info('Actively requesting for:')
	for group in requested_data:
		if requested_data[group]['multiline'] == True:
			for data in requested_data[group]['data_in_group']:
				fieldname = requested_data[group]['data_in_group'][data]['fieldname']
				logger.info(fieldname)
				fieldnames.append(fieldname)
		else:
			fieldname = requested_data[group]['fieldname']
			logger.info(fieldname)
			fieldnames.append(fieldname)	
	[response_ids.append(requested_data[group]['response_id']) \
	for group in requested_data if requested_data[group]['response_id'] not in response_ids] 
	
# Determine fieldnames and response IDs of data passively listened for, if any
if passive_data:
	logger.info('Pasively listening for:')
	for group in passive_data:
		fieldname = passive_data[group]['fieldname']
		logger.info(fieldname)
		fieldnames.append(fieldname)
	[response_ids.append(passive_data[group]['response_id']) \
	for group in passive_data if passive_data[group]['response_id'] not in response_ids] 

# Show IDs being collected
logger.info('Collecting IDs:')
for id in response_ids:
	logger.info(hex(id))

# Setup csv file for logging to local storage
filename = '/home/pi/python-projects/obd/logs/log_decoded'
logger.info("Data file exists? " + str(path.isfile(filename + '.csv')))
if not path.isfile(filename + '.csv'):
	logger.info('Writing new file')
	outfile = open(filename + '.csv','w', newline="")
	# Write fieldnames to CSV and add header
	writer = csv.DictWriter(outfile, fieldnames=fieldnames)
	writer.writeheader()
else:
	logger.info('Appending file')
	outfile = open(filename + '.csv','a', newline="")
	writer = csv.DictWriter(outfile, fieldnames=fieldnames)

# Any code below here will stop if kill signal is received at power down
try:
	# Initialize serial connection to BLE Scanner, any code below here will fail if OBD scanner not found
	logger.info('Connecting to ELM327 device...')
	scanner_attempts = 0
	while True:	
		try:
			# Let OBD scaner connect
			time.sleep(5)

			ser = serial.Serial('/tmp/ttyBLE', 38400, timeout=2)
			logger.info('Connected to ELM327')
			
			# Clear reader output
			ser.readline()

			# Sending initialization commands
			logger.info('Initializating scanner for data collection')

			# Reset ELM327 interface
			ser.write(b'AT Z\r')
			time.sleep(0.1)
			ser_bytes = ser.readline()
			if ser_bytes == b'':
				logger.info('No response')
				raise OSError

			logger.info(ser_bytes)

			# Turn headers on
			ser.write(b'AT H1\r')
			time.sleep(0.1)
			ser_bytes = ser.readline()
			logger.info(ser_bytes)

			# Display DLC
			ser.write(b'AT D1\r')
			time.sleep(0.1)
			ser_bytes = ser.readline()
			logger.info(ser_bytes)

			# Turn off echo
			ser.write(b'AT E0\r')
			time.sleep(0.1)
			ser_bytes = ser.readline()
			logger.info(ser_bytes)

			# If provided, send flow control message
			if config['settings']['flow_control_message']:
				logger.info('Flow control message provided:')
				flow_bytes = []
				if config['settings']['flow_control_message']['b1'] is not None:
					flow_b1 = config['settings']['flow_control_message']['b1']
					logger.info('Flow Control B1: {}'.format(hex(flow_b1)[2:].zfill(2)))
					flow_bytes.append(flow_b1)
				if config['settings']['flow_control_message']['b2'] is not None:
					flow_b2 = config['settings']['flow_control_message']['b2']
					logger.info('Flow Control B2: {}'.format(hex(flow_b2)[2:].zfill(2)))
					flow_bytes.append(flow_b2)
				if config['settings']['flow_control_message']['b3'] is not None:
					flow_b3 = config['settings']['flow_control_message']['b3']
					logger.info('Flow Control B3: {}'.format(hex(flow_b3)[2:].zfill(2)))
					flow_bytes.append(flow_b3)
				if config['settings']['flow_control_message']['b4'] is not None:
					flow_b4 = config['settings']['flow_control_message']['b4']
					logger.info('Flow Control B4: {}'.format(hex(flow_b4)[2:].zfill(2)))
					flow_bytes.append(flow_b4)
				string_flow_bytes = [hex(int)[2:].zfill(2) for int in flow_bytes]
				string_of_flow_bytes = "".join(string_flow_bytes)

				logger.info('Sending flow control message')
				flow_message = b'AT FC SD %b\r' % string_of_flow_bytes.encode()
				logger.info(flow_message)
				ser.write(flow_message)
				time.sleep(0.1)
				ser_bytes = ser.readline()
				logger.info(ser_bytes)

			count = 1
			timeout = 0

			# Main loop
			while True:
				# Create empty decoded data dictionary
				decoded_dict = {}
				date_time_obj = datetime.now()
				timestamp = date_time_obj.strftime('%Y-%m-%d %H:%M:%S')
				decoded_dict = {"date_time": timestamp}
				decoded_dict.update({"ev_id": ev_id})
				decoded_dict.update({"count": count})
				if requested_data:
					for group in requested_data:
						logger.info('Group is: {}'.format(group))
						# Get CAN message from config
						request_id = requested_data[group]['request_id']
						request_bytes = []
						if requested_data[group]['b1']:
							request_b1 = requested_data[group]['b1']
							logger.info('B1: {}'.format(hex(request_b1)[2:].zfill(2)))
							request_bytes.append(request_b1)
						if requested_data[group]['b2']:
							request_b2 = requested_data[group]['b2']
							logger.info('B2: {}'.format(hex(request_b2)[2:].zfill(2)))
							request_bytes.append(request_b2)
						if requested_data[group]['b3']:
							request_b3 = requested_data[group]['b3']
							logger.info('B3: {}'.format(hex(request_b3)[2:].zfill(2)))
							request_bytes.append(request_b3)
						if requested_data[group]['b4']:
							request_b4 = requested_data[group]['b4']
							logger.info('B4: {}'.format(hex(request_b4)[2:].zfill(2)))
							request_bytes.append(request_b4)

						# Convert bytes to hex string
						string_request_bytes = [hex(int)[2:].zfill(2) for int in request_bytes]
						string_of_bytes = " ".join(string_request_bytes)

						# Send request
						can_request(request_id, string_of_bytes)

						# Check if response will be multiline
						if requested_data[group]['multiline'] == True:
							logger.info('Multiline')
							# Read response and check ID
							multiline_response = ser.readline()
							if multiline_response == b'':
								logger.info('No response')
								raise OSError
								break

							logger.info(multiline_response)
							can_id = multiline_response[:3].decode()
							logger.info('Received ID: {}'.format(can_id))

							# Separate multiline response into lines
							lines = multiline_response.decode().splitlines()

							# Add relevent data into list of raw data
							raw = []
							for i in range(len(lines)):
								logger.info('Line {} is: {}'.format(i+1, lines[i][6:]))
								raw.append(bytes.fromhex(lines[i][6:])) 

							# Decoding
							for data in requested_data[group]['data_in_group']:
								# Get name of data to be decoded
								fieldname = requested_data[group]['data_in_group'][data]['fieldname']
								
								# Get formula
								formula = requested_data[group]['data_in_group'][data]['decoding']

								# Check whether value was given a test for its sign
								if requested_data[group]['data_in_group'][data]['sign'] == True:
									# Evaluate decoding formula
									decoded = eval(formula)

									logger.info('Decoded value needs to have sign checked')
									sign_check = requested_data[group]['data_in_group'][data]['sign_check']
									if_true = requested_data[group]['data_in_group'][data]['if_true']
									if_false = requested_data[group]['data_in_group'][data]['if_false']

									sign_test = eval(sign_check)
									if sign_test == True:
										logger.info('Sign test came back true')
										decoded = eval(if_true)
									else:
										logger.info('Sign test came back false')
										decoded = eval(if_false)

									logger.info('{}: {}'.format(fieldname, decoded))
								else:
									# Evaluate decoding formula
									decoded = eval(formula)
									# logger.info response
									logger.info('{}: {}'.format(fieldname, decoded))


								# Add decoded data to dictionary
								decoded_dict.update({fieldname: decoded})

						else: # Singleline response
							logger.info('Singleline')
							# Read response and check ID
							response = ser.readline()
							if response == b'':
								logger.info('No response')
								raise OSError
							logger.info(response)
							can_id = response[:3].decode()
							logger.info('Received ID: {}'.format(can_id))

							# Get data from response
							data = response[6:].decode()

							# Clean data - remove any unwanted characters
							data = data.replace('\r', '')
							data = data.replace('>', '')

							# Convert data to hex
							logger.info('Data is: {}'.format(data))
							raw = bytes.fromhex(data)
							logger.info('Raw from hex is: {}'.format(raw))

							# Decoding
							# Get fieldname of data
							fieldname = requested_data[group]['fieldname']

							# Get decoding formula
							formula = requested_data[group]['decoding']
							
							# Check whether value was given a test for its sign
							if requested_data[group]['sign'] == True:
								# Evaluate decoding formula
								decoded = eval(formula)

								logger.info('Decoded value needs to have sign checked')
								sign_check = requested_data[group]['sign_check']
								if_true = requested_data[group]['if_true']
								if_false = requested_data[group]['if_false']

								sign_test = eval(sign_check)
								if sign_test == True:
									logger.info('Sign test came back true')
									decoded = eval(if_true)
								else:
									logger.info('Sign test came back false')
									decoded = eval(if_false)

								logger.info('{}: {}'.format(fieldname, decoded))
							else:
								# Evaluate decoding formula
								decoded = eval(formula)
								# logger.info response
								logger.info('{}: {}'.format(fieldname, decoded))

							# Add decoded data to dictionary
							decoded_dict.update({fieldname: decoded})

				if passive_data:
					for group in passive_data:
						logger.info('Group is: {}'.format(group))

				# Log decoded data to csv
				writer.writerow(decoded_dict)
				logger.info(writer)

				# Increase count
				count = count + 1

				# Check internet connection
				ssid = os.popen('iwgetid').read()
				if ssid:
					logger.info('Connected to WIFI with SSID: {}'.format(ssid))
					# Try to upload data
					if using_vpn == True:
						uploaded = upload_vpn(outfile, fieldnames, filename)
					else:
						uploaded = upload(mysql_hostname, mysql_username, mysql_main_database, mysql_password, outfile, fieldnames, filename)
				else:
					uploaded = False
					logger.info('No connection, skipping upload')
				
				# Check if data was uploaded, if so clear log file, if not then append
				logger.info("Data uploaded? {}".format(uploaded))
				if uploaded == True:
					logger.info('Uploaded, clearing log file')
					outfile = open(filename + '.csv','w', newline="")
					writer = csv.DictWriter(outfile, fieldnames=fieldnames)
					writer.writeheader()
					count = 1
				else:
					logger.info('Data not uploaded, keeping old file')
					outfile = open(filename + '.csv','a', newline="")
					writer = csv.DictWriter(outfile, fieldnames=fieldnames)
					
				# Clear reader output
				ser.readline()

				# Wait sampling time
				time.sleep(sampling_time)

		except OSError as os_error:
			logger.info(os_error)
			scanner_attempts = scanner_attempts + 1
			logger.info('Cannot find ELM327 device, (attempt {}) will try again'.format(scanner_attempts))
			time.sleep(5)
			# Once scanner cannot be reached 3 times, restart the BLE autoconnect service
			if scanner_attempts == 3:
				logger.info('Reached 3 scanner attempts, restarting BLE autoconnect')
				os.system('systemctl --user restart ble-autoconnect-user.service')
				time.sleep(1)
				scanner_attempts = 0
			# elif scanner_attempts == 6:
			# 	logger.info('Too many scanner attempts, restarting bluetooth service')
			# 	os.system('sudo service bluetooth restart')
			# 	time.sleep(1)
			# 	scanner_attempts = 0

except KeyboardInterrupt:
	# Catch keyboard interrupt/service stop 
	logger.info('\n\rStarting shutdown sequence')
	
	# Try to upload data
	logger.info('\n\rTrying to upload data')
	
	# Check internet connection
	ssid = os.popen('iwgetid').read()
	if ssid:
		logger.info('Connected to WIFI with SSID: {}'.format(ssid))
		# Try to upload data
		if using_vpn == True:
			uploaded = upload_vpn(outfile, fieldnames, filename)
		else:
			uploaded = upload(mysql_hostname, mysql_username, mysql_main_database, mysql_password, outfile, fieldnames, filename)
	else:
		uploaded = False
		logger.info('No connection, skipping upload')

	# Check if data was uploaded, if so clear log file, if not then append
	logger.info("Data uploaded? {}".format(uploaded))
	if uploaded == True:
		logger.info('Uploaded, clearing log file')
		outfile = open(filename + '.csv','w', newline="")
		writer = csv.DictWriter(outfile, fieldnames=fieldnames)
		writer.writeheader()
		count = 1
	else:
		logger.info('Data not uploaded, keeping old file')
		outfile = open(filename + '.csv','a', newline="")
		writer = csv.DictWriter(outfile, fieldnames=fieldnames)

	# Closing logger file
	outfile.close() 

	# Close serial connection
	ser.close()
	logger.info('\n\rShutdown sequence complete, shutting down.')
	exit()

