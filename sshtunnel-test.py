#!/usr/bin/env python3

# Comment made locally!
# Comment made remotely!
# Comment made on laptop
# this should be in feature-add-ev-id branch

# importing modules
import serial
import logging
import time
from threading import Thread
import yaml
import os
from os import path
import csv
from datetime import datetime
import mysql.connector as sql
from mysql.connector import Error
import csv
import configparser
from sshtunnel import SSHTunnelForwarder

# Define function for data upload with VPN
def upload_vpn(outfile):
	print('Starting VPN')
	try:
		os.system('sudo bash /home/pi/python-projects/obd/scripts/start-openfortivpn.sh')
		print('Started openfortivpn')
		time.sleep(2)
	except Exception as err:
		print("Couldn't start openfortivpn")
		print(err)
		

	print('Creating SSH tunnel')
	with SSHTunnelForwarder(
		(ssh_host, ssh_port),
		ssh_username=ssh_user,
		remote_bind_address=(mysql_hostname, mysql_port)) as tunnel:
		print(tunnel.local_bind_address)
		print('Connecting to MySQL through SSH tunnel')
		mydb = None
		try:
			mydb = sql.connect(host='localhost', user=mysql_username, password=mysql_password, database=mysql_main_database, port=tunnel.local_bind_port, auth_plugin = 'mysql_native_password')
			curs = mydb.cursor()
			print('Connected to MySQL')
			if mydb.is_connected():
				print('Connected to MySQL database, uploading')
				outfile.close()

				query_placeholders = ', '.join(['%s'] * len(fieldnames))
				query_columns = ', '.join(fieldnames)

				with open(filename + '.csv') as csv_data:
					cursor = mydb.cursor()
					reader = csv.reader(csv_data)
					next(reader)
					
					for row in reader:
						print(row)
						for i in row:
							breaker = False
							if not i:
								print('Found empty element, skipping MySQL upload')
								breaker = True
								break
						if breaker:
							continue
						print('Uploading row')
						insert_query = '''INSERT INTO %s (%s) VALUES (%s) ''' %(table_name, query_columns, query_placeholders)
						cursor.execute(insert_query, row)
						mydb.commit()

				print('Uploaded, clearing log file')
				outfile = open(filename + '.csv','w', newline="")
				writer = csv.DictWriter(outfile, fieldnames=fieldnames)
				writer.writeheader()
				count = 1

		except Error as e:
			print(e)
			outfile = open(filename + '.csv','a', newline="")
			writer = csv.DictWriter(outfile, fieldnames=fieldnames)
			print('Skipping upload, keeping old file')

		finally:
			if mydb is not None and mydb.is_connected():
				mydb.close()

# Load database INI file
db_config = configparser.ConfigParser()
db_config.read("/home/pi/python-projects/obd/config/database.ini")
print('Found database file')

# Check if using VPN for connection to database
using_vpn = db_config['SETTINGS'].getboolean('Using_VPN')
if using_vpn == True:
	print('Using VPN, will use SSH tunnel')
	ssh_host = db_config['SERVER']['SSH_Host']
	ssh_user = db_config['SERVER']['SSH_User']
	ssh_port = int(db_config['SERVER']['SSH_Port'])

# Database information
database = db_config['MYSQL']
mysql_hostname = database['MySQL_Hostname']
mysql_username = database['MySQL_Username']
mysql_password = database['MySQL_Password']
mysql_main_database = database['MySQL_Database']
mysql_port = int(database['MYSQL_Port'])
table_name = database['MySQL_Table']

# Load YAML config data, assign necessary variables
print('Loading config...')
with open('/home/pi/python-projects/obd/config/obd_logger_config.yaml') as f:
	config = yaml.load(f, Loader=yaml.FullLoader)
	
# Get EV ID
ev_id = config['settings']['ev_id']
# Timeout attemps
timeout_attempts = config['settings']['timeout_attempts']
# Sampling time
sampling_time = config['settings']['timeout_attempts']
# Determine any data being requested
requested_data = config['data']['requested_data']
# Determine any data being passively listened for
passive_data = config['data']['passive_data']

fieldnames = ['date_time', 'ev_id', 'count']
response_ids = []
# Determine fieldnames and response IDs of data requested, if any
if requested_data:
	print('Actively requesting for:')
	for group in requested_data:
		if requested_data[group]['multiline'] == True:
			for data in requested_data[group]['data_in_group']:
				fieldname = requested_data[group]['data_in_group'][data]['fieldname']
				print(fieldname)
				fieldnames.append(fieldname)
		else:
			fieldname = requested_data[group]['fieldname']
			print(fieldname)
			fieldnames.append(fieldname)	
	[response_ids.append(requested_data[group]['response_id']) \
	for group in requested_data if requested_data[group]['response_id'] not in response_ids] 
	
# Determine fieldnames and response IDs of data passively listened for, if any
if passive_data:
	print('Pasively listening for:')
	for group in passive_data:
		fieldname = passive_data[group]['fieldname']
		print(fieldname)
		fieldnames.append(fieldname)
	[response_ids.append(passive_data[group]['response_id']) \
	for group in passive_data if passive_data[group]['response_id'] not in response_ids] 

# Show IDs being collected
print('Collecting IDs:')
for id in response_ids:
	print(hex(id))

# Setup csv file for logging to local storage
filename = '/home/pi/python-projects/obd/logs/log_decoded'
print("Data file exists? " + str(path.isfile(filename + '.csv')))
if not path.isfile(filename + '.csv'):
	print('Writing new file')
	outfile = open(filename + '.csv','w', newline="")
	# Write fieldnames to CSV and add header
	writer = csv.DictWriter(outfile, fieldnames=fieldnames)
	writer.writeheader()
else:
	print('Appending file')
	outfile = open(filename + '.csv','a', newline="")
	writer = csv.DictWriter(outfile, fieldnames=fieldnames)

if using_vpn == True:
	upload_vpn(outfile)
else:
	upload(mysql_hostname, mysql_username, mysql_main_database, mysql_password, outfile, fieldnames, filename)
# Closing logger file
outfile.close() 