#!/bin/bash 
 
echo "`date -Is` Starting WiFi check"

SSID=$(/sbin/iwgetid --raw) 

if [[ "$SSID" = "public@unb" ]]
then
    echo "`date -Is` On public@unb network, will run wifi-autoaccept.py to accept captive network"
    python3 /home/pi/python-projects/obd/scripts/wifi-autoaccept.py
fi 

echo "`date -Is` WiFi check finished"
