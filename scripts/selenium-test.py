import time
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver', options=options)

url = "https://www.geeksforgeeks.org/"

driver.get(url)

button = driver.find_element_by_class_name("slide-out-btn")

button.click()