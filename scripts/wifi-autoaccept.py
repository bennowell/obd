from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
import os

website_link='http://google.com'
# username=""
# password=""

# element_for_username=""
# element_for_password=""
element_for_submit="/html/body/form/section/aside/div[1]/input"

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

#driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver', chrome_options=options)
driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver', options=options)
#s=Service(ChromeDriverManager().install())
#driver = webdriver.Chrome(service=s)

driver.get((website_link))

#driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver')

accepted = False
while accepted is False:
        try:
                # username_element = browser.find_element_by_name(element_for_username)
                # username_element.send_keys(username)
                # password_element  = browser.find_element_by_name(element_for_password)
                # password_element.send_keys(password)
                # signInButton = driver.find_element_by_xpath(element_for_submit)
                # signInButton.click()
                #driver.find_element_by_xpath("//*[contains(@value,'Yes, I agree')]").click()
                #button = driver.find_element(By.XPATH, "//*[contains(@value,'Yes, I agree')]")
                #accept_button = driver.find_element(By.XPATH, "//*[contains(@value,'Yes, I agree')]")
                accept_button = driver.find_element_by_class_name('primary')
                print('Found element')
                accept_button.click()
                print('Accepted terms')
                #button.click()
                #driver.get("https://www.python.org")
                #print(driver.title)
        except Exception:
                # Catch error
                print("Some error occured, trying XPATH")
                accept_button = driver.find_element(By.XPATH, "//*[contains(@value,'Yes, I agree')]")
                print('Found element')
                accept_button.click()
                print('Accepted terms')
        else:
                print('Wifi reconnect complete')
                accepted = True
        
        time.sleep(5)
        
print('WiFi accepted')

