#!/bin/bash 
 
echo "`date -Is` Starting WiFi check"

SSID=$(/sbin/iwgetid --raw) 

if [[ -z "$SSID" ]] 
then
    echo "`date -Is` WiFi interface is down, trying to reconnect"
    sudo ifconfig wlan0 down
    sleep 10
    sudo ifconfig wlan0 up 
elif [[ "$SSID" = "public@unb" ]]
then
    echo "On public@unb network, will run autoaccept.sh"
    python3 /home/pi/python-projects/obd/scripts/wifi-autoaccept.py
fi 

echo "`date -Is` WiFi check finished"
