#!/bin/bash

SSID=$(/sbin/iwgetid --raw)

if [ "$SSID" = "BELL087" ]
then
	echo "On public@unb network, will run autoaccept.sh"
	python3 /home/pi/python-projects/obd/scripts/wifi-autoaccept.py
fi

echo 'WiFi check finished'