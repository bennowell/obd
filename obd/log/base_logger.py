import logging
from logging.handlers import RotatingFileHandler

def create_logger():
	# Create and configure logger
	logger = logging.getLogger()
	logger.setLevel(logging.INFO)

	# Create logging file
	logfile = "/home/pi/python-projects/obd/logs/obd_logger_output.log"
	file = RotatingFileHandler(logfile, mode='a', maxBytes=5*1024*1024,
		backupCount=2, encoding=None, delay=0)
	file.setLevel(logging.INFO)
	fileformat = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
	file.setFormatter(fileformat)
	logger.addHandler(file)

	# Send logs to console
	ch = logging.StreamHandler()
	ch.setLevel(logging.INFO)
	ch_format = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
	ch.setFormatter(ch_format)
	logger.addHandler(ch)

	logging.info('Logging file created')


