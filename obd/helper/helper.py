import logging
import configparser
import yaml
from datetime import datetime

# Load database INI config file
def read_db_config():
	logging.info('Reading database config...')
	db_config = configparser.ConfigParser()
	db_config.read("/home/pi/python-projects/obd/config/database.ini")
	logging.info('Found database config file')
	return db_config

# Load CAN YAML config file
def read_can_config():
	logging.info('Reading CAN config...')
	with open('/home/pi/python-projects/obd/config/obd_logger_config.yaml') as f: 
		can_config = yaml.load(f, Loader=yaml.FullLoader)
		logging.info('Found CAN config file')
		return can_config

def get_string_of_bytes(group, requested_data):
	logging.info('Group is: {}'.format(group))
	# Get CAN message from config
	request_id = requested_data[group]['request_id']
	request_bytes = []
	if requested_data[group]['b1']:
		request_b1 = requested_data[group]['b1']
		logging.info('B1: {}'.format(hex(request_b1)[2:].zfill(2)))
		request_bytes.append(request_b1)
	if requested_data[group]['b2']:
		request_b2 = requested_data[group]['b2']
		logging.info('B2: {}'.format(hex(request_b2)[2:].zfill(2)))
		request_bytes.append(request_b2)
	if requested_data[group]['b3']:
		request_b3 = requested_data[group]['b3']
		logging.info('B3: {}'.format(hex(request_b3)[2:].zfill(2)))
		request_bytes.append(request_b3)
	if requested_data[group]['b4']:
		request_b4 = requested_data[group]['b4']
		logging.info('B4: {}'.format(hex(request_b4)[2:].zfill(2)))
		request_bytes.append(request_b4)

	# Convert bytes to hex string
	string_request_bytes = [hex(int)[2:].zfill(2) for int in request_bytes]
	string_of_bytes = " ".join(string_request_bytes)
	return request_id, string_of_bytes

def get_list_of_raw_multiline_data(multiline_response):
	logging.info('Multiline response')
	#logging.info(multiline_response)
	can_id = multiline_response[:3].decode()
	logging.info('Received ID: {}'.format(can_id))

	# Separate multiline response into lines
	lines = multiline_response.decode().splitlines()

	# Add relevent data into list of raw data
	raw = []
	for i in range(len(lines)):
		logging.info('Line {} is: {}'.format(i+1, lines[i][6:]))
		try:
			raw.append(bytes.fromhex(lines[i][6:]))
		except ValueError:
			logging.info('Incomplete line of data, not including it') 
	return raw

def get_raw_singleline_data(response):
	logging.info('Singleline')
	logging.info(response)
	can_id = response[:3].decode()
	logging.info('Received ID: {}'.format(can_id))

	# Get data from response
	data = response[6:].decode()

	# Clean data - remove any unwanted characters
	data = data.replace('\r', '')
	data = data.replace('>', '')

	# Convert data to hex
	logging.info('Hex data is: {}'.format(data))
	raw = bytes.fromhex(data)
	logging.info('Raw from hex is: {}'.format(raw))

	return raw

def decode_raw(sign_check_required, formula, sign_check, if_true, if_false, raw, fieldname):
	# Check whether value was given a test for its sign
	if sign_check_required == True:
		# Evaluate decoding formula
		decoded = eval(formula)

		logging.info('Decoded value needs to have sign checked')
		
		sign_test = eval(sign_check)
		if sign_test == True:
			logging.info('Sign test came back true')
			decoded = eval(if_true)
		else:
			logging.info('Sign test came back false')
			decoded = eval(if_false)

	else:
		# Evaluate decoding formula
		decoded = eval(formula)

	logging.info('{}: {}'.format(fieldname, decoded))
	return decoded
