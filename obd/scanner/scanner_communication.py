import logging
import serial
import time

# Define function for initializing OBD2 scanner
def initialize_scanner(can_config):
   logging.info('Connecting to ELM327 device...')

   # Give BLE-Serial time to connect
   time.sleep(5)

   ser = serial.Serial('/tmp/ttyBLE', 38400, timeout=1)
   logging.info('Connected to ELM327')
         
   # Clear reader output
   ser.readline()

   # Sending initialization commands
   logging.info('Initializating scanner for data collection')

   # Reset ELM327 interface
   ser.write(b'AT Z\r')
   time.sleep(0.1)
   ser_bytes = ser.readline()
   if ser_bytes == b'':
      logging.info('No response')
      raise OSError

   logging.info(ser_bytes)

   # Turn headers on
   ser.write(b'AT H1\r')
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)

   # Display DLC
   ser.write(b'AT D1\r')
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)

   # Turn off echo
   ser.write(b'AT E0\r')
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)

   # If provided, send flow control message
   if can_config['settings']['flow_control_message']:
      logging.info('Flow control message provided:')
      flow_bytes = []
      if can_config['settings']['flow_control_message']['b1'] is not None:
         flow_b1 = can_config['settings']['flow_control_message']['b1']
         logging.info('Flow Control B1: {}'.format(hex(flow_b1)[2:].zfill(2)))
         flow_bytes.append(flow_b1)
      if can_config['settings']['flow_control_message']['b2'] is not None:
         flow_b2 = can_config['settings']['flow_control_message']['b2']
         logging.info('Flow Control B2: {}'.format(hex(flow_b2)[2:].zfill(2)))
         flow_bytes.append(flow_b2)
      if can_config['settings']['flow_control_message']['b3'] is not None:
         flow_b3 = can_config['settings']['flow_control_message']['b3']
         logging.info('Flow Control B3: {}'.format(hex(flow_b3)[2:].zfill(2)))
         flow_bytes.append(flow_b3)
      if can_config['settings']['flow_control_message']['b4'] is not None:
         flow_b4 = can_config['settings']['flow_control_message']['b4']
         logging.info('Flow Control B4: {}'.format(hex(flow_b4)[2:].zfill(2)))
         flow_bytes.append(flow_b4)
      string_flow_bytes = [hex(int)[2:].zfill(2) for int in flow_bytes]
      string_of_flow_bytes = "".join(string_flow_bytes)

      logging.info('Sending flow control message')
      flow_message = b'AT FC SD %b\r' % string_of_flow_bytes.encode()
      logging.info(flow_message)
      ser.write(flow_message)
      time.sleep(0.1)
      ser_bytes = ser.readline()
      logging.info(ser_bytes)

      return ser

# Define function for writing to OBD2 scanner
def can_request(request_id, string_of_bytes, ser):
   # Set CAN ID
   logging.info('CAN ID: {}'.format(hex(request_id)))
   at_sh_message = b'AT SH %b\r' % hex(request_id)[2:].encode()
   logging.info(at_sh_message)
   ser.write(at_sh_message)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)
    
   at_fc_sh_message = b'AT FC SH %b\r' % hex(request_id)[2:].encode()
   logging.info(at_fc_sh_message)
   ser.write(at_fc_sh_message)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)
   
   # Set flow control mode
   logging.info('Setting flow control mode')
   flow_control_mode = b'AT FC SM 1\r'
   logging.info(flow_control_mode)
   ser.write(flow_control_mode)
   time.sleep(0.1)
   ser_bytes = ser.readline()
   logging.info(ser_bytes)
   
   # Send CAN request
   logging.info('Sending message: {}'.format(string_of_bytes))
   msg_bytes = b'%b\r' % string_of_bytes.encode()
   logging.info(msg_bytes)
   ser.write(msg_bytes)
   time.sleep(0.1)

   response = ser.readline()
   return response



