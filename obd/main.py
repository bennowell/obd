import logging
import serial
import time
import csv
from os import path
import os
from datetime import datetime
from scanner import scanner_communication
from database import database_uploading
from log import base_logger
from helper import helper

# Create logging
base_logger.create_logger()

def main():
	# Read database config file
	db_config = helper.read_db_config()

	# Assign database information
	database = db_config['MYSQL'] 
	mysql_hostname = database['MySQL_Hostname']
	mysql_username = database['MySQL_Username']
	mysql_password = database['MySQL_Password']
	mysql_main_database = database['MySQL_Database']
	mysql_port = int(database['MYSQL_Port'])
	table_name = database['MySQL_Table']
	logging.info('MySQL Info - Hostname: %s, Username: %s, Database: %s, Port: %s', mysql_hostname, mysql_username, mysql_main_database, mysql_port)

	# Check if using VPN for connection to database
	using_vpn = db_config['SETTINGS'].getboolean('Using_VPN')
	if using_vpn == True:
		logging.info('Using VPN, will use SSH tunnel')
		ssh_host = db_config['SERVER']['SSH_Host']
		ssh_user = db_config['SERVER']['SSH_User']
		ssh_port = int(db_config['SERVER']['SSH_Port'])
		logging.info('SSH Info - Host: %s, User: %s, Port: %s', ssh_host, ssh_user, ssh_port)
		
	# Read CAN config file
	can_config = helper.read_can_config()
		
	# Assign CAN config information
	ev_id = can_config['settings']['ev_id']
	timeout_attempts = can_config['settings']['timeout_attempts']
	sampling_time = can_config['settings']['sampling_time']
	requested_data = can_config['data']['requested_data']
	passive_data = can_config['data']['passive_data']

	# Start fieldnames
	fieldnames = ['date_time', 'ev_id', 'count']

	# Determine fieldnames and response IDs of data requested, if any
	response_ids = []
	if requested_data:
		logging.info('Actively requesting for:')
		for group in requested_data:
			if requested_data[group]['multiline'] == True:
				for data in requested_data[group]['data_in_group']:
					fieldname = requested_data[group]['data_in_group'][data]['fieldname']
					logging.info(fieldname)
					fieldnames.append(fieldname)
			else:
				fieldname = requested_data[group]['fieldname']
				logging.info(fieldname)
				fieldnames.append(fieldname)	
		[response_ids.append(requested_data[group]['response_id']) \
		for group in requested_data if requested_data[group]['response_id'] not in response_ids] 
		
	# Determine fieldnames and response IDs of data passively listened for, if any
	if passive_data:
		logging.info('Pasively listening for:')
		for group in passive_data:
			fieldname = passive_data[group]['fieldname']
			logging.info(fieldname)
			fieldnames.append(fieldname)
		[response_ids.append(passive_data[group]['response_id']) \
		for group in passive_data if passive_data[group]['response_id'] not in response_ids] 

	# Show IDs being collected
	logging.info('Collecting IDs:')
	for id in response_ids:
		logging.info(hex(id))

	trip_test = can_config['settings']['trip_test']
	if trip_test:
		logging.info('Trip test given:')
		trip_started = False
		for group in trip_test:
			trip_test_fieldname = trip_test[group]['fieldname']
			logging.info(trip_test_fieldname)	
			trip_test_response_id = trip_test[group]['response_id']
			in_park = trip_test[group]['in_park'] 

	# Setup csv file for logging to local storage
	filename = '/home/pi/python-projects/obd/logs/log_decoded'
	logging.info("Data file exists? " + str(path.isfile(filename + '.csv')))
	if not path.isfile(filename + '.csv'):
		logging.info('Writing new file')
		outfile = open(filename + '.csv','w', newline="")
		# Write fieldnames to CSV and add header
		writer = csv.DictWriter(outfile, fieldnames=fieldnames)
		writer.writeheader()
	else:
		logging.info('Appending file')
		outfile = open(filename + '.csv','a', newline="")
		writer = csv.DictWriter(outfile, fieldnames=fieldnames)

	# Any code below here will stop if kill signal is received
	try:
		scanner_attempts = 0
		restarts = 0
	
		while True:
			# Any code below here will stop if scanner connection is dropped
			try:
				ser = scanner_communication.initialize_scanner(can_config)

				count = 1
				timeout = 1

				# Main Loop
				while True:
					# Create empty decoded data dictionary
					decoded_dict = {}
					date_time_obj = datetime.now()
					timestamp = date_time_obj.strftime('%Y-%m-%d %H:%M:%S')
					decoded_dict = {"date_time": timestamp}
					decoded_dict.update({"ev_id": ev_id})
					decoded_dict.update({"count": count})

					if requested_data:
						for group in requested_data:
							# Get message ID and string of bytes to send
							request_id, string_of_bytes = helper.get_string_of_bytes(group, requested_data)

							while True:
								# Send message
								response = scanner_communication.can_request(request_id, string_of_bytes, ser)

								# Check response
								if response == b'':
									logging.info('Attempt {} of {}: No response, trying again'.format(timeout, timeout_attempts))
									timeout = timeout + 1
									if timeout >= timeout_attempts:
										raise OSError
									
								else:
									logging.info('Got response')
									timeout = 1
									break

							# Check if response will be multiline
							if requested_data[group]['multiline'] == True:
								raw = helper.get_list_of_raw_multiline_data(response)
								# Decoding
								for data in requested_data[group]['data_in_group']:
									logging.info(data)
									# Get name of data to be decoded
									fieldname = requested_data[group]['data_in_group'][data]['fieldname']
									# Get formula
									formula = requested_data[group]['data_in_group'][data]['decoding']
									# Get sign test value
									sign_check_required = requested_data[group]['data_in_group'][data]['sign']
									if sign_check_required == True:
										sign_check = requested_data[group]['data_in_group'][data]['sign_check']
										if_true = requested_data[group]['data_in_group'][data]['if_true']
										if_false = requested_data[group]['data_in_group'][data]['if_false']
									else:
										sign_check = None
										if_true = None
										if_false = None

									decoded = helper.decode_raw(sign_check_required, formula, sign_check, if_true, if_false, raw, fieldname)

									# Add decoded data to dictionary
									decoded_dict.update({fieldname: decoded})
				
							else: # Singleline response
								raw = helper.get_raw_singleline_data(response)
								logging.info('Raw in main is: {}'.format(raw))
								# Decoding
								# Get fieldname of data
								fieldname = requested_data[group]['fieldname']

								# Get decoding formula
								formula = requested_data[group]['decoding']

								# Get sign test value
								sign_check_required = requested_data[group]['sign']
								if sign_check_required == True:
									sign_check = requested_data[group]['sign_check']
									if_true = requested_data[group]['if_true']
									if_false = requested_data[group]['if_false']
								else:
									sign_check = None
									if_true = None
									if_false = None
				
								decoded = helper.decode_raw(sign_check_required, formula, sign_check, if_true, if_false, raw, fieldname)

								# Add decoded data to dictionary
								decoded_dict.update({fieldname: decoded})
				
					# Log decoded data to csv
					writer.writerow(decoded_dict)
					
					# Increase count since data upload
					count = count + 1

					# Test for auto-shutdown trip test
					# Car gear will be checked each iteration
					# trip_started will remain False until car is detected out of park, then will become True
					# After trip_started is True, car gear will be checked repeatedly until determined to be in park again
					# Once in park again, shutdown sequence will be triggered 
					if trip_test:
						for group in trip_test:
							# Get message ID and string of bytes to send
							request_id, string_of_bytes = helper.get_string_of_bytes(group, trip_test)

							while True:
								# Send message
								response = scanner_communication.can_request(request_id, string_of_bytes, ser)

								# Check response
								if response == b'':
									logging.info('Attempt {} of {}: No response, trying again'.format(timeout, timeout_attempts))
									timeout = timeout + 1
									if timeout >= timeout_attempts:
										raise OSError
									
								else:
									logging.info('Got response')
									timeout = 1
									break

							raw = helper.get_raw_singleline_data(response)
							logging.info('Raw in main is: {}'.format(raw))
							# Decoding
							# Get fieldname of data
							fieldname = trip_test[group]['fieldname']

							# Get decoding formula
							formula = trip_test[group]['decoding']

							# Get sign test value
							sign_check_required = trip_test[group]['sign']
							if sign_check_required == True:
								sign_check = trip_test[group]['sign_check']
								if_true = trip_test[group]['if_true']
								if_false = trip_test[group]['if_false']
							else:
								sign_check = None
								if_true = None
								if_false = None

							decoded = helper.decode_raw(sign_check_required, formula, sign_check, if_true, if_false, raw, fieldname)

							# Check whether car is still in park, if it isn't, then trip_started becomes True
							if decoded == in_park:
								logging.info('In park')
							else:
								trip_started = True
								logging.info('Trip has started')

							# Check whether trip has started, if so check if back in park, and start shutdown sequence if so.
							if trip_started:
								logging.info('Checking if back in park')
								if decoded == in_park:
									logging.info('Back in park. Starting shutdown sequence')
									raise KeyboardInterrupt
								else:
									logging.info('Not in park, trip not finished, will continue data collection.')

					# Check internet connection, if connected try to upload data
					ssid = os.popen('iwgetid').read()
					if ssid:
						logging.info('Connected to WIFI with SSID: {}'.format(ssid))
						# Check if using VPN
						if using_vpn == True:
							# Start VPN
							database_uploading.start_vpn()
							# Create SSH Tunnel to MySQL
							logging.info(mysql_port)
							tunnel = database_uploading.create_ssh_tunnel(ssh_host, ssh_port, ssh_user, mysql_port)
							try:
								tunnel.start()
								# Get new MySQL port on SSH tunnel
								db_port = tunnel.local_bind_port
								logging.info('SSH Tunnel Port: {}'.format(db_port))
							except:
								logging.info('No tunnel made')
								db_port = None
							
							# Set host to local host
							mysql_hostname = 'localhost'
						else:
							db_port = mysql_port
							
						# Connect to MySQL
						mydb = database_uploading.connect_to_mysql(mysql_hostname, mysql_username, mysql_password, mysql_main_database, db_port)
						# Attempt to upload CSV
						uploaded = database_uploading.upload_csv_to_mysql(mydb, filename, outfile, fieldnames, table_name)

					else:
						uploaded = False
						logging.info('No WIFI connection, skipping upload')
				
					# Check if data was uploaded, if so clear log file, if not then append
					logging.info("Data uploaded? {}".format(uploaded))
					if uploaded == True:
						logging.info('Uploaded, clearing log file')
						outfile = open(filename + '.csv','w', newline="")
						writer = csv.DictWriter(outfile, fieldnames=fieldnames)
						writer.writeheader()
						count = 1
					else:
						logging.info('Data not uploaded, keeping old file')
						outfile = open(filename + '.csv','a', newline="")
						writer = csv.DictWriter(outfile, fieldnames=fieldnames)
						
					# Clear reader output
					ser.readline()

					# Wait sampling time
					time.sleep(sampling_time)

			except OSError as os_error:
				logging.info(os_error)
				scanner_attempts = scanner_attempts + 1
				logging.info('Cannot find ELM327 device, (attempt {}) will try again'.format(scanner_attempts))
				time.sleep(5)
				# Once scanner cannot be reached 3 times, restart the BLE autoconnect service
				if scanner_attempts == 3:
					logging.info('Reached 3 scanner attempts, restarting BLE autoconnect')
					os.system('systemctl --user restart ble-autoconnect-user.service')
					time.sleep(1)
					scanner_attempts = 0
					restarts = restarts + 1

				if restarts == 5:
					logging.info('Maximum bluetooth restarts reached. Rebooting.')
					os.system('sudo reboot')
	
	except KeyboardInterrupt:
		# Catch keyboard interrupt/service stop 
		logging.info('\n\rStarting shutdown sequence')
	
		# Try to upload data
		logging.info('\n\rTrying to upload data')
		
		# Check internet connection, if connected try to upload data
		ssid = os.popen('iwgetid').read()
		if ssid:
			logging.info('Connected to WIFI with SSID: {}'.format(ssid))
			# Check if using VPN
			if using_vpn == True:
				# Start VPN
				database_uploading.start_vpn()
				# Create SSH Tunnel to MySQL
				logging.info(mysql_port)
				tunnel = database_uploading.create_ssh_tunnel(ssh_host, ssh_port, ssh_user, mysql_port)
				try:
					tunnel.start()
					# Get new MySQL port on SSH tunnel
					db_port = tunnel.local_bind_port
					logging.info('SSH Tunnel Port: {}'.format(db_port))
				except:
					logging.info('No tunnel made')

				# Set host to local host
				mysql_hostname = 'localhost'
			else:
				db_port = mysql_port

			# Connect to MySQL
			mydb = database_uploading.connect_to_mysql(mysql_hostname, mysql_username, mysql_password, mysql_main_database, db_port)
			# Attempt to upload CSV
			uploaded = database_uploading.upload_csv_to_mysql(mydb, filename, outfile, fieldnames, table_name)

		else:
			uploaded = False
			logging.info('No WIFI connection, skipping upload')
	
		# Check if data was uploaded, if so clear log file, if not then append
		logging.info("Data uploaded? {}".format(uploaded))
		if uploaded == True:
			logging.info('Uploaded, clearing log file')
			outfile = open(filename + '.csv','w', newline="")
			writer = csv.DictWriter(outfile, fieldnames=fieldnames)
			writer.writeheader()
			count = 1
		else:
			logging.info('Data not uploaded, keeping old file')
			outfile = open(filename + '.csv','a', newline="")
			writer = csv.DictWriter(outfile, fieldnames=fieldnames)
		
		# Close logging file
		outfile.close()

		# Close serial connection
		ser.close()
		logging.info('\n\rShutdown sequence complete, shutting down.')
		os.system('sudo shutdown -h now')
		#exit()

if __name__ == '__main__':
	main()