import logging
import os
import time
import sshtunnel
from sshtunnel import SSHTunnelForwarder
import mysql.connector as sql
from mysql.connector import Error
import csv
import configparser

def start_vpn():
	logging.info('Starting VPN')
	try:
		os.system('sudo bash /home/pi/python-projects/obd/scripts/start-openfortivpn.sh')
		time.sleep(20)
		logging.info('Started openfortivpn')
	except Exception as vpn_error:
		logging.info(vpn_error)
		logging.info("Skipping data upload: couldn't start openfortivpn")
	
def create_ssh_tunnel(ssh_host, ssh_port, ssh_user, port):
	logging.info('Creating SSH tunnel with host: {} and port: {}'.format(ssh_host, ssh_port))
	try:
		tunnel = SSHTunnelForwarder((ssh_host, ssh_port), ssh_username=ssh_user, remote_bind_address=(ssh_host, port))
		return tunnel
	except (Exception, KeyboardInterrupt) as e:
			logging.info(e)
			logging.info("Skipping data upload: couldn't establish SSH tunnel")

def connect_to_mysql(mysql_hostname, mysql_username, mysql_password, mysql_main_database, mysql_port):
	try:
		mydb = sql.connect(host=mysql_hostname, user=mysql_username, password=mysql_password, database=mysql_main_database, port=mysql_port, auth_plugin = 'mysql_native_password')
		logging.info('Connected to MySQL') 
		return mydb
	except Error as e:
		logging.info(e)
		logging.info("Skipping data upload: couldn't connect to MySQL")

def upload_csv_to_mysql(mydb, filename, outfile, fieldnames, table_name):
	uploaded = False
	try:
		if mydb.is_connected():
			logging.info('Connected to MySQL database, uploading')
			outfile.close()

			query_placeholders = ', '.join(['%s'] * len(fieldnames))
			query_columns = ', '.join(fieldnames)

			with open(filename + '.csv') as csv_data:
				cursor = mydb.cursor()
				reader = csv.reader((line.replace('\0','') for line in csv_data))
				next(reader)
						
				for row in reader:
					logging.info(row)
					for i in row:
						breaker = False
						if not i:
							logging.info('Found empty element, skipping MySQL upload')
							breaker = True
							break
					if breaker:
						continue
					logging.info('Uploading row')
					insert_query = '''INSERT INTO %s (%s) VALUES (%s) ''' %(table_name, query_columns, query_placeholders)
					cursor.execute(insert_query, row)
					mydb.commit()

			logging.info('Data uploaded to MySQL')
			uploaded = True

	except AttributeError:
		logging.info("No attribute is.connected()")

	except Error as e:
		logging.info(e)
		logging.info("Skipping data upload: problem uploading csv")

	finally:
		if mydb is not None and mydb.is_connected():
			mydb.close()

	return uploaded

if __name__ == '__main__':
	# Create logger
	logger = logging.getLogger()
	logger.setLevel(logging.INFO)
	# Send logs to console
	ch = logging.StreamHandler()
	ch.setLevel(logging.INFO)
	ch_format = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
	ch.setFormatter(ch_format)
	logger.addHandler(ch)
	
	# Get database info
	logging.info('Reading database config...')
	db_config = configparser.ConfigParser()
	db_config.read("/home/pi/python-projects/obd/config/database.ini")
	logging.info('Found database config file')

	# Assign database information
	database = db_config['MYSQL'] 
	mysql_hostname = database['MySQL_Hostname']
	mysql_username = database['MySQL_Username']
	mysql_password = database['MySQL_Password']
	mysql_main_database = database['MySQL_Database']
	mysql_port = int(database['MYSQL_Port'])
	table_name = database['MySQL_Table']
	logging.info('MySQL Info - Hostname: %s, Username: %s, Database: %s, Port: %s', mysql_hostname, mysql_username, mysql_main_database, mysql_port)

	# Assign sever information
	server = db_config['SERVER']

	settings = db_config['SETTINGS']
	using_vpn = settings.getboolean('Using_VPN')
	if using_vpn == True:
		#start_vpn()
		ssh_hostname = server['SSH_Host']
		ssh_user = server['SSH_User']
		ssh_port = int(server['SSH_Port'])
		tunnel = create_ssh_tunnel(ssh_hostname, ssh_port, ssh_user, mysql_port)
		tunnel.start()
		logging.info(tunnel.local_bind_address)
		db_port = tunnel.local_bind_port
		logging.info(db_port)
		mysql_hostname = 'localhost'
	else:
		db_port = mysql_port

	mydb = connect_to_mysql(mysql_hostname, 'ev_dataloader', 'ev_dataloader_password', 'test', db_port)
	filename = 'test'
	outfile = open(filename + '.csv','a', newline="")
	fieldnames = ['column_one', 'column_two']
	mysql_database = 'test'
	table_name = 'test_table'
	uploaded = upload_csv_to_mysql(mydb, filename, outfile, fieldnames, table_name)
	logger.info(uploaded)
