pyserial==3.5
logging==0.4.9.6
pyyaml==6.0
mysql-connector-python==8.0.29
configparser==5.2.0
sshtunnel==0.4.0
selenium==4.1.0